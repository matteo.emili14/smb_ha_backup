<?php
error_reporting(1);

$smb_host = "";
$username = "";
$password = "";
$smb_share = "";
$backup_folder = "";

$files = array();
foreach(glob($backup_folder."*.tar") as $file) {
	$files[] = $file;
}

usort($files, function($a,$b){
	return filemtime($a) - filemtime($b);
});

$files_name_array = array();
foreach($files as $value)
{
	array_push($files_name_array, end(explode("/", $value)));
}

$newest_file = end(explode("/", $files[(count($files)-1)]));
$data = file_get_contents($backup_folder.$newest_file, "r");

// Create new state:
$state = smbclient_state_new();

// Initialize the state with workgroup, username and password:
smbclient_state_init($state, null, $username, $password);

//Copy the latest backup
$new_file = smbclient_open($state, "smb://".$smb_host."/".$smb_share."/".$newest_file, "w", 0777);
$write_result = smbclient_write($state, $new_file, $data);

if(!$write_result)
	echo "Errore durante la scrittura del file!\n";
else
	echo "Copia avvenuto con successo!\n";

smbclient_close($state, $new_file);

//Delete the oldest backup
$folder_scan = smbclient_opendir($state, "smb://".$smb_host."/".$smb_share."/");

// Loop over the directory contents, delete not synced files
while (($entry = smbclient_readdir($state, $folder_scan)) !== false) {
	if(!in_array($entry['name'], $files_name_array) && $entry['type'] == "file")
		smbclient_unlink($state, "smb://".$smb_host."/".$smb_share."/".$entry['name']);
}

echo "Sincronizzazione avvenuta!\n";

// Close the directory handle:
smbclient_closedir($state, $folder_scan);

// Free the state:
smbclient_state_free($state);

?>