#!/bin/bash
sudo apt -y install php-pear php-smbclient
wget "https://gitlab.com/matteo.emili14/smb_ha_backup/raw/master/backup_on_smb.php"
cp ./backup_on_smb.php ~/
chmod +x ~/backup_on_smb.php
crontab -l > mycron
echo "0 4 * * * /usr/bin/php ~/backup_on_smb.php > ~/backup_on_smb.txt" >> mycron
crontab mycron
rm mycron
echo "Installazione completata con successo!"
echo "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
echo "${RED}Ricordati di aprire il file backup_on_smb.php nella tua cartella utente ed inserire i parametri per il funzionamento.${NC}"
echo "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
